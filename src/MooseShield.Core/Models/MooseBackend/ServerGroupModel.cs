﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MooseShield.Core.Models.MooseBackend
{
    public class ServerGroupModel
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
    }
}