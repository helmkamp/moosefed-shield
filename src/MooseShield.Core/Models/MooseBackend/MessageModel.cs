﻿namespace MooseShield.Core.Models.MooseBackend
{
    public class MessageModel
    {
        public string Message { get; set; }

        public MessageModel() { }

        public MessageModel(string message)
        {
            Message = message;
        }
    }
}