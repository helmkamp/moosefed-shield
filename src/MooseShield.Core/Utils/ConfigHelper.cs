﻿using System;
using System.Configuration;
using System.Reflection;
using System.Text.RegularExpressions;

namespace MooseShield.Core.Utils
{
    public class ConfigHelper
    {
        /// <summary>
        /// Retrieve application setting in the form of a string.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(value))
                throw new NullReferenceException(string.Format("The value for '{0}' is empty or key is missing from web.config", key));

            return value;
        }

        /// <summary>
        /// Retrieve application setting in the form of a string.
        /// If the string is empty or missing, we return an empty string instead.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSettingNullable(string key)
        {
            string value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return value;
        }

        /// <summary>
        /// Retrieve an e-mail address from application settings.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSettingEmail(string key)
        {
            string value = GetAppSetting(key);

            if (!Regex.IsMatch(value, @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$", RegexOptions.IgnoreCase))
                throw new FormatException(string.Format("The value for key '{0}' in web.config is not a valid e-mail address.", key));

            return value;
        }

        /// <summary>
        /// Retrieve application setting in the form of an integer.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetAppSettingInt(string key)
        {
            return int.Parse(GetAppSetting(key));
        }

        /// <summary>
        /// Retrieve application setting in the form of boolean
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetAppSettingBool(string key)
        {
            return bool.Parse(GetAppSetting(key));
        }

        /// <summary>
        /// Retrieve a connection string from the web.config
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            // Get the value from the web.config
            var connString = ConfigurationManager.ConnectionStrings[key];

            if (connString == null || string.IsNullOrEmpty(connString.ConnectionString))
                throw new NullReferenceException("Configuration value for '" + key + "' is null, empty or missing from web.config!");

            return connString.ConnectionString;
        }
    }
}