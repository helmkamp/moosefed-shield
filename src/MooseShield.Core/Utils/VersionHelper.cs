﻿namespace MooseShield.Core.Utils
{
    public class VersionHelper
    {
        public static string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}