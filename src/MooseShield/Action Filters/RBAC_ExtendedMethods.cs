﻿using EVE.SingleSignOn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MooseShield.Action_Filters;


//Get requesting user's roles/permissions from database tables
public static class RBAC_ExtendedMethods 
    {
        //private static SSOCharacter _user = EVE.SingleSignOn.SSOClient.GetCharacter;

        public static bool HasRole(string role, SSOCharacter character)
        {
            bool bFound = false;
            try
            {
                //Check if the requesting user has the specified role
                bFound = new RBACUser(character.CharacterName).HasRole(role);
            }
            catch { }
            return bFound;
        }

        public static bool HasRoles(string roles, SSOCharacter character)
        {
            bool bFound = false;
            try
            {
                //Check if the requesting user has any of the specified roles
                //Make sure you separate the roles using ; (ie "Sales Manager;Sales Operator"
                bFound = new RBACUser(character.CharacterName).HasRoles(roles);
            }
            catch { }
            return bFound;
        }

        public static bool HasPermission(string permission, SSOCharacter character)
        {
            bool bFound = false;
            try
            {
                //Check if the requesting user has the specified application permission
                bFound = new RBACUser(character.CharacterName).HasPermission(permission);
            }
            catch { }
            return bFound;
        }

        public static bool IsSysAdmin(SSOCharacter character)
        {
            bool bIsSysAdmin = false;
            try
            {
                //controller = _user.CharacterName;
                //Check if the requesting user has the System Administrator privilege
                bIsSysAdmin = new RBACUser(character.CharacterName).IsSysAdmin;
            }
            catch { }
            return bIsSysAdmin;
        }
    }
