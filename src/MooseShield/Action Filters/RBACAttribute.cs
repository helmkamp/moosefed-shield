﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MooseShield.Action_Filters
{
    public class RBACAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //Create permission string based on the requested controoler name and action name in the format 'controller-action'
            string requiredPermission = String.Format("{0}-{1}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName);

            //Create an instance of the custom user authorization object passing requesting user's CharacterName into constructor
            var loggedInCharacter = EVE.SingleSignOn.SSOClient.GetCharacter;
            try
            {
                RBACUser requestingUser = new RBACUser(loggedInCharacter.CharacterName);
                //Check if the requesting user has the permission to run the controller's action
                if (!requestingUser.HasPermission(requiredPermission) & !requestingUser.IsSysAdmin)
                {
                    //User doesn't have the required permission and is not a SysAdmin, return our custom “401 Unauthorized” access error
                    //Since we are setting filterContext.Result to contain an ActionResult page, the controller's action will not be run.
                    //The custom “401 Unauthorized” access error will be returned to the browser in response to the initial request.
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Index" }, { "controller", "Unauthorised" } });
                }                
            }
            catch
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Index" }, { "controller", "Home" } });
            }
            

            //If the user has the permission to run the controller's action, then filterContext.Result will be uninitialized and
            //executing the controller's action is dependant on whether filterContext.Result is uninitialized.
        }
    }
}