﻿using System;
using System.Collections.Generic;
using MooseShield.Models;
using System.Linq;

namespace MooseShield.Action_Filters
{
    public class RBACUser
    {
        public string Username { get; set; }
        public bool IsSysAdmin { get; private set; }
        public int UserId { get; set; }
        private List<UserRole> Roles = new List<UserRole>();

        public RBACUser(string _username)
        {
            this.Username = _username;
            this.IsSysAdmin = false;
            GetDatabaseUserRolesPermissions();
        }

        private void GetDatabaseUserRolesPermissions()
        {
            using (TSAuthContext _data = new TSAuthContext("Local"))
            {
                User _user = _data.Users.Where(u => u.CharacterName == this.Username).FirstOrDefault();
                if(_user != null)
                {
                    this.UserId = _user.UserID;
                    foreach(Role _role in _user.Roles)
                    {
                        UserRole _userRole = new UserRole { RoleId = _role.RoleId, RoleName = _role.Name };
                        foreach (Permission _permission in _role.Permissions)
                        {
                            _userRole.Permissions.Add(new RolePermission { PermissionId = _permission.PermissionId, PermissionName = _permission.PermissionName });
                        }
                        this.Roles.Add(_userRole);

                        if (!this.IsSysAdmin)
                            this.IsSysAdmin = _role.IsSysAdmin;
                    }
                }
            }
        }

        public bool HasPermission(string requiredPermission)
        {
            bool bFound = false;
            foreach( UserRole role in this.Roles)
            {
                bFound = (role.Permissions.Where(p => p.PermissionName.ToLower() == requiredPermission.ToLower()).ToList().Count > 0);
                if (bFound)
                    break;
            }
            return bFound;
        }

        public bool HasRole(string role)
        {
            return (Roles.Where(p => p.RoleName == role).ToList().Count > 0);
        }

        public bool HasRoles(string roles)
        {
            bool bFound = false;
            string[] _roles = roles.ToLower().Split(';');
            foreach(UserRole role in this.Roles)
            {
                try
                {
                    bFound = _roles.Contains(role.RoleName.ToLower());
                    if (bFound)
                        return bFound;
                }
                catch(Exception) { }
            }
            return bFound;
        }
    }

    public class UserRole
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public List<RolePermission> Permissions = new List<RolePermission>();
    }

    public class RolePermission
    {
        public int PermissionId { get; set; }
        public string PermissionName { get; set; }
    }
}