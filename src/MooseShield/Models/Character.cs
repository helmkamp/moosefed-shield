﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MooseShield.Models
{
    public class Character
    {
        [JsonProperty]
        public Corporation Corporation { get; set; }
        [JsonProperty]
        public string Name { get; set; }
    }
}