﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MooseShield.Models.XmlCharacter
{
    [Serializable]
    public class Result
    {
        public string CharacterID { get; set; }
        public string CharacterName { get; set; }
        public string Race { get; set; }
        public string BloodLineID { get; set; }
        public string BloodLine { get; set; }
        public string AncestryID { get; set; }
        public string Ancestry { get; set; }
        public string CorporationID { get; set; }
        public string Corporation { get; set; }
        public DateTime CorporationDate { get; set; }
        public string AllianceID { get; set; }
        public string Alliance { get; set; }
        public DateTime AllianceDate { get; set; }
        public string SecurityStatus { get; set; }
    }
}