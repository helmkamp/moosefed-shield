namespace MooseShield.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TeamSpeakUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TeamSpeakUser()
        {
            ServerGroups = new HashSet<ServerGroup>();
        }

        public int TeamSpeakUserID { get; set; }
        [Required,RegularExpression(@".{5,}\=$",ErrorMessage = "Do you even Unique ID BRO?! Be a doll and RTFM!")]
        public string UniqueID { get; set; }

        public string ServerGroup { get; set; }

        public string CharacterOwnerHash { get; set; }

        public string CharacterName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServerGroup> ServerGroups { get; set; }
    }
}
