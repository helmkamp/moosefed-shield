﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MooseShield.Models
{
    public class Corporation
    {
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public long Id { get; set; }
        [JsonProperty]
        public bool IsNpc { get; set; }
    }
}