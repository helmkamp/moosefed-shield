namespace MooseShield.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Roles = new HashSet<Role>();
            //Inactive = false;
        }

        [Key]
        public int UserID { get; set; }

        public long CharacterID { get; set; }

        public string CharacterName { get; set; }

        public string CharacterOwnerHash { get; set; }

       // public bool? Inactive { get; set; }

        public string Corporation { get; set; }

        public string Alliance { get; set; }

        public DateTime CreatedAt { get; set; }
        
        

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Role> Roles { get; set; }
    }
}
