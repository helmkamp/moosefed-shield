namespace MooseShield.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ServerGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ServerGroup()
        {
            TeamSpeakUsers = new HashSet<TeamSpeakUser>();
        }

        public int ServerGroupID { get; set; }

        public string ServerGroupName { get; set; }

        public int TSServerGroupID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeamSpeakUser> TeamSpeakUsers { get; set; }
    }
}
