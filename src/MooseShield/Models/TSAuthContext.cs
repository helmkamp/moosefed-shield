namespace MooseShield.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TSAuthContext : DbContext
    {
        public TSAuthContext()
            : base("name=Local")
        {
        }
        
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<ServerGroup> ServerGroups { get; set; }
        public virtual DbSet<TeamSpeakUser> TeamSpeakUsers { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public TSAuthContext(string connectionString) : base(connectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permission>()
                .HasMany(e => e.Roles)
                .WithMany(e => e.Permissions)
                .Map(m => m.ToTable("RolePermissions"));

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UserRoles"));

            modelBuilder.Entity<ServerGroup>()
                .HasMany(e => e.TeamSpeakUsers)
                .WithMany(e => e.ServerGroups)
                .Map(m => m.ToTable("TeamSpeakUserServerGroups"));
        }
    }
}
