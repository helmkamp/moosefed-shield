﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooseShield.Action_Filters;
using MooseShield.Models;

namespace MooseShield.Controllers
{
    [RBAC]
    public class ServerGroupsController : Controller
    {
        private TSAuthContext db = new TSAuthContext();

        // GET: ServerGroups
        public ActionResult Index()
        {
            return View(db.ServerGroups.ToList());
        }

        // GET: ServerGroups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServerGroup serverGroup = db.ServerGroups.Find(id);
            if (serverGroup == null)
            {
                return HttpNotFound();
            }
            return View(serverGroup);
        }

        // GET: ServerGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ServerGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServerGroupID,ServerGroupName,TSServerGroupID")] ServerGroup serverGroup)
        {
            if (ModelState.IsValid)
            {
                db.ServerGroups.Add(serverGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(serverGroup);
        }

        // GET: ServerGroups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServerGroup serverGroup = db.ServerGroups.Find(id);
            if (serverGroup == null)
            {
                return HttpNotFound();
            }
            return View(serverGroup);
        }

        // POST: ServerGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServerGroupID,ServerGroupName,TSServerGroupID")] ServerGroup serverGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serverGroup).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(serverGroup);
        }

        // GET: ServerGroups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServerGroup serverGroup = db.ServerGroups.Find(id);
            if (serverGroup == null)
            {
                return HttpNotFound();
            }
            return View(serverGroup);
        }

        // POST: ServerGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServerGroup serverGroup = db.ServerGroups.Find(id);
            db.ServerGroups.Remove(serverGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
