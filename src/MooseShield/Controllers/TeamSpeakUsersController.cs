﻿using EVE.SingleSignOn;
using EVE.SingleSignOn.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MooseShield.Action_Filters;
using MooseShield.Models;

namespace MooseShield.Controllers
{
    //[RBAC]
    public class TeamSpeakUsersController : Controller
    {
        private TSAuthContext db = new TSAuthContext("Local");
        private ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: TeamSpeakUsers
        [RBACAttribute]
        public ActionResult Index()
        {
            return View(db.TeamSpeakUsers.OrderBy(x => x.CharacterName).ToList());
        }

        // GET: TeamSpeakUsers/Details/5
        [RBAC]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamSpeakUser teamSpeakUser = db.TeamSpeakUsers.Find(id);
            if (teamSpeakUser == null)
            {
                return HttpNotFound();
            }
            return View(teamSpeakUser);
        }


        // GET: TeamSpeakUsers/Create
        public ActionResult Create()
        {
            if (SSOClient.IsLoggedIn)
            {
                var loggedInCharacter = SSOClient.GetCharacter;
                RBACUser requestingUser = new RBACUser(loggedInCharacter.CharacterName);
                if (requestingUser.HasRole("Administrator"))
                {
                    ViewData["Role"] = true;
                    return View();
                }
                ViewData["Role"] = false;
                return View();
            }


            return RedirectToAction("Index", "Home");
        }

        // POST: TeamSpeakUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Create([Bind(Include = "TeamSpeakUserID,UniqueID")] TeamSpeakUser teamSpeakUser)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.TeamSpeakUsers.Where(u => u.UniqueID == teamSpeakUser.UniqueID).Single();
                    ViewBag.Message = "You have already been granted access to the Moose Federation TeamSpeak server.";
                }
                catch (Exception e)
                {
                    var loggedInCharacter = SSOClient.GetCharacter;
                    var currentUser = db.Users.Where(u => u.CharacterOwnerHash == loggedInCharacter.CharacterOwnerHash).Single();
                    teamSpeakUser.CharacterName = currentUser.CharacterName;

                    switch (currentUser.Corporation)
                    {
                        case "Moosearmy":
                            teamSpeakUser.ServerGroup = "7,17";
                            break;

                        case "Moosearmy Heavy Industries":
                            teamSpeakUser.ServerGroup = "7,17";
                            break;
                        case "Saints Of Havoc":
                            teamSpeakUser.ServerGroup = "7,21";
                            break;
                        default:
                            teamSpeakUser.ServerGroup = "7";
                            break;
                    }


                    // Retrieve the list of server group IDs
                    int[] ids = Array.ConvertAll(teamSpeakUser.ServerGroup.Split(','), int.Parse);
                    List<int> groupIds = ids.ToList();

                    // Assign groups to the user
                    if (await ServerGroupAssign.ServerGroupAssignment(teamSpeakUser.UniqueID, groupIds))
                    {
                        _log.Info($"Successfully added {groupIds.Count} groups to {teamSpeakUser.CharacterName} ({teamSpeakUser.UniqueID})");
                        teamSpeakUser.CharacterOwnerHash = loggedInCharacter.CharacterOwnerHash;
                        // Store in DB
                        db.TeamSpeakUsers.Add(teamSpeakUser);
                        db.SaveChanges();

                        return RedirectToAction("Success", "Home");
                    }

                    _log.Error($"Could not assign groups to {teamSpeakUser.CharacterName} ({teamSpeakUser.UniqueID})");
                    ModelState.AddModelError("", "It looks like you're not connected to the TS server. RTFM!");
                    return View();
                }

            }

            return View(teamSpeakUser);
        }

        // GET: TeamSpeakUsers/Edit/5
        [RBAC]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamSpeakUser teamSpeakUser = db.TeamSpeakUsers.Find(id);
            if (teamSpeakUser == null)
            {
                return HttpNotFound();
            }

            return View(teamSpeakUser);
        }

        // POST: TeamSpeakUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [RBAC]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Edit([Bind(Include = "TeamSpeakUserID,UniqueID,ServerGroup")] TeamSpeakUser teamSpeakUser)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Convert the updated server group list to an int[]
                    int[] newServerGroups = Array.ConvertAll(teamSpeakUser.ServerGroup.Split(','), int.Parse);
                    if (newServerGroups != null)
                    {
                        _log.Debug($"newServerGroups created: {newServerGroups}");
                    }

                    if (newServerGroups.Contains(6))
                    {
                        int removeSix = 6;
                        newServerGroups = newServerGroups.Where(v => v != removeSix).ToArray();
                    }
                    // Grab the current server group list, convert it to an int[]
                    TeamSpeakUser sg = null;
                    int[] _currentServerGroup = { };
                    _log.Debug("sg and _currentServerGroup created.");
                    using (var context = new TSAuthContext("Local"))
                    {
                        sg = context.TeamSpeakUsers.Where(s => s.UniqueID == teamSpeakUser.UniqueID).Single();
                        _currentServerGroup = Array.ConvertAll(sg.ServerGroup.Split(','), int.Parse);
                        _log.Debug($"_currentServerGroup: {_currentServerGroup.ToString()}");
                        if (newServerGroups.Count() < _currentServerGroup.Count())
                        {
                            List<int> _deleteServerGroups = _currentServerGroup.Except(newServerGroups).ToList();

                            await ServerGroupAssign.ServerGroupDelete(teamSpeakUser.UniqueID, _deleteServerGroups);
                            _log.Debug("Server Groups Deleted.");
                        }

                    }


                    // Get the difference between the arrays and assign it to _serverGroups
                    List<int> _serverGroups = newServerGroups.Except(_currentServerGroup).ToList();

                    SSOCharacter loggedInCharacter = SSOClient.GetCharacter;
                    User currentUser = db.Users.Where(u => u.CharacterOwnerHash == loggedInCharacter.CharacterOwnerHash).Single();
                    teamSpeakUser.CharacterName = currentUser.CharacterName;
                    teamSpeakUser.CharacterOwnerHash = loggedInCharacter.CharacterOwnerHash;
                    _log.Debug($"Char Name: {teamSpeakUser.CharacterName} : COH: {teamSpeakUser.CharacterOwnerHash}");
                    db.Entry(teamSpeakUser).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    _log.Debug("Database updated.");
                    await ServerGroupAssign.ServerGroupAssignment(teamSpeakUser.UniqueID, _serverGroups);
                    _log.Debug("TeamSpeak update.");
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    _log.Error("Encountered issues while trying to update user's server group.", e);
                }
            }
            return View(teamSpeakUser);
        }

        // GET: TeamSpeakUsers/Delete/5
        [RBAC]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamSpeakUser teamSpeakUser = db.TeamSpeakUsers.Find(id);
            if (teamSpeakUser == null)
            {
                return HttpNotFound();
            }
            return View(teamSpeakUser);
        }

        // POST: TeamSpeakUsers/Delete/5
        [RBAC]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TeamSpeakUser teamSpeakUser = db.TeamSpeakUsers.Find(id);
            db.TeamSpeakUsers.Remove(teamSpeakUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ServerGroupListPartial()
        {
            List<ServerGroup> sgList = db.ServerGroups.OrderBy(s => s.ServerGroupID).ToList();

            return PartialView("_ServerGroupList", sgList);
        }
    }
}
