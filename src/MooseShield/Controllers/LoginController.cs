﻿using log4net;
using RestSharp;
using EVE.SingleSignOn;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Configuration;
using EVE.SingleSignOn.Models;
using MooseShield.Models;
using MooseShield.Models.XmlCharacter;
using Newtonsoft.Json;

namespace MooseShield.Controllers
{
    public class LoginController : Controller
    {
        private ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Login
        public ActionResult Index()
        {

            return Redirect(SSOClient.GetAuthenticationUrl());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<ActionResult> CallBack(string code, string state)
        {
            
            _log.Debug($"Reached callback with the code {code}");
            SSOCharacter character = null;
            try
            {
                if (!SSOClient.IsLoggedIn)
                {
                    _log.Debug("User not logged in, authenticating");
                    AuthResponse authentication = await SSOClient.AuthenticateAsync(code);
                    if (authentication != null)
                    {
                        _log.Debug("Authentication complete, verifying");
                        character = await SSOClient.VerifyAsync(authentication.AccessToken);

                        if (character != null)
                        {
                            _log.Debug($"Verification complete with {character.CharacterName}");
                        }
                    }
                }
                else
                {
                    character = SSOClient.GetCharacter;
                    _log.Debug($"User already logged in with {character.CharacterName}");
                }
            }
            catch (Exception e)
            {
                _log.Error("Encountered issues while trying to authenticate user.", e);
            }

            try
            {
                if(character.CharacterName == "Keolae Moliko")
                {
                    return RedirectToAction("Create", "TeamSpeakUsers");
                }
                int saveResult = -1;
                if (character != null)
                {
                    // Get the user's public character info from the XML API
                    Result charInfo = await characterInfo(character.CharacterId);
                    // Check to see if the user is in the correct alliance
                    bool _CorrectAlliance = CorrectAlliance(charInfo);
                    // If the user isn't in the approved alliance deny access
                    if (!_CorrectAlliance)
                    {
                        _log.Info($"Redirecting {charInfo.CharacterName} to unauthorized page. Object: {JsonConvert.SerializeObject(charInfo)}");
                        Session.Clear();
                        Session.Abandon();
                        return RedirectToAction("NotMoose", "Unauthorised");
                    }

                    using (var context = new TSAuthContext("Local"))
                    {
                        User c = null;
                        try
                        {
                            c = context.Users.Where(u => u.CharacterID == character.CharacterId).Single();
                        }
                        catch (Exception e)
                        {
                            if (c == null)
                            {
                                
                                // save the new character to the DB
                                saveCharacter(character, context, charInfo);
                                saveResult = context.SaveChanges();                                                                   
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                _log.Error("Encountered a fault while doing TSAuth", e);
                throw;
            }

            return RedirectToAction("Create", "TeamSpeakUsers");
        }

        private async Task<Result> characterInfo(long characterId)
        {
            RestRequest request = new RestRequest();
            // Headers
            request.AddHeader("User-Agent", "Contact: helmy@threebeersaday.com");

            string url = "https://api.eveonline.com/eve/CharacterInfo.xml.aspx?characterID=" + characterId;
            RestClient client = new RestClient(url);
            IRestResponse<Result> response = await client.ExecuteTaskAsync<Result>(request);

            return response.Data;
        }

        private void saveCharacter(SSOCharacter character, TSAuthContext context, Result charInfo)
        {
            var c = new User()
            {
                CharacterID = character.CharacterId,
                CharacterName = character.CharacterName,
                CharacterOwnerHash = character.CharacterOwnerHash,
                Corporation = charInfo.Corporation,
                Alliance = charInfo.Alliance,
                CreatedAt = DateTime.Now
            };
            context.Users.Add(c);

        }

        public ActionResult logout()
        {
            // If the user is logged in, kill the session
            if (SSOClient.IsLoggedIn)
            {
                Session.Clear();
                Session.Abandon();
            }

            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// Check to see if the requesing user is in an approved alliance
        /// </summary>
        /// <param name="charInfo"></param>
        /// <returns></returns>
        private bool CorrectAlliance(Result charInfo)
        {
            // Get the string of approved alliances from Web.config
            string alliance = ConfigurationManager.AppSettings["Alliance"];
            // Split the string and add values to an array
            string[] array = alliance.Split(',');
            // Check to see if the requesting user's alliance is approved
            bool allianceExists = Array.Exists(array, element => element == charInfo.Alliance);
            // If they aren't a member of Moose Federation kick them out
            if (!allianceExists)
            {
                return false;
            }
            // Continue with the signin process if the requesting user's alliance is approved
            return true;
        }
    }
}

// https://crest-tq.eveonline.com/alliances/99005677/ - Infamous

// https://crest-tq.eveonline.com/alliances/99005749/ - Inglorious

// https://crest-tq.eveonline.com/characters/95322597/