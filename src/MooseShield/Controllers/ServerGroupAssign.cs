﻿using log4net;
using MooseShield.Core.Models.MooseBackend;
using MooseShield.Core.Utils;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TentacleSoftware.TeamSpeakQuery;
using TentacleSoftware.TeamSpeakQuery.ServerQueryResult;

namespace MooseShield.Controllers
{
    public class ServerGroupAssign
    {
        private static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// Assign users to groups
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="serverGroupIds"></param>
        /// <returns></returns>
        public static async Task<bool> ServerGroupAssignment(string uniqueId, List<int> groupIds)
        {
            // Generate a request for adding user to groups
            RestRequest request = new RestRequest(Method.PUT);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(new { UniqueId = uniqueId, ServerGroupIds = groupIds });

            // Generate the URL for service
            UriBuilder builder = new UriBuilder(ConfigHelper.GetAppSettingNullable("MooseBackend.Url"))
            {
                Path = "/svc/TeamSpeak/"
            };

            // Initialize the client
            RestClient client = new RestClient(builder.Uri);

            // Call the service
            IRestResponse<MessageModel> response = await client.ExecuteTaskAsync<MessageModel>(request);

            // process the return info
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                _log.Info(response.Data.Message);
                return true;
            }

            _log.Error(response.Data.Message);
            return false;
        }

        public static async Task<bool> ServerGroupDelete(string uniqueId, List<int> groupIds)
        {
            // Generate a request for adding user to groups
            RestRequest request = new RestRequest(Method.DELETE);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(new { UniqueId = uniqueId, ServerGroupIds = groupIds });

            // Generate the URL for service
            UriBuilder builder = new UriBuilder(ConfigHelper.GetAppSettingNullable("MooseBackend.Url"))
            {
                Path = "/svc/TeamSpeak/"
            };

            // Initialize the client
            RestClient client = new RestClient(builder.Uri);

            // Call the service
            IRestResponse<MessageModel> response = await client.ExecuteTaskAsync<MessageModel>(request);

            // process the return info
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                _log.Info(response.Data.Message);
                return true;
            }

            _log.Error(response.Data.Message);
            return false;
        }

        public static async Task<List<ServerGroupModel>> GetServerGroupList()
        {
            // Generate a request for adding user to groups
            RestRequest request = new RestRequest(Method.OPTIONS);
            request.AddHeader("Content-Type", "application/json");

            // Generate the URL for service
            UriBuilder builder = new UriBuilder(ConfigHelper.GetAppSettingNullable("MooseBackend.Url"))
            {
                Path = "/svc/TeamSpeak/"
            };

            // Initialize the client
            RestClient client = new RestClient(builder.Uri);

            // Call the service
            IRestResponse<List<ServerGroupModel>> response = await client.ExecuteTaskAsync<List<ServerGroupModel>>(request);

            // process the return info
            if (response.StatusCode == HttpStatusCode.OK)
            {
                _log.Info($"Retrieved {response.Data.Count} groups from server");
                return response.Data;
            }

            _log.Error("Could not retrieve server groups from server");
            return new List<ServerGroupModel>();
        }
    }
}