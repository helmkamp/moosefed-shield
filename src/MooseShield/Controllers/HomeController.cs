﻿using System.Web.Mvc;
using EVE.SingleSignOn;
using MooseShield.Action_Filters;

namespace MooseShield.Controllers
{

    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            //User charOH = (User)Session["_characterKey"];
            //var character = _context.Users.Where(c => c.CharacterOwnerHash == charOH).Single();
            var charOH = SSOClient.GetCharacter;


            return View(charOH);
        }

        [RBAC]
        public ActionResult Contact()
        {
            //int[] serverGroup = { 10, 11, 15, 16 };
            //ServerGroupAssignController assignment = new ServerGroupAssignController();
            //await assignment.getServerGroupList();
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Success()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Instructions()
        {
            return View();
        }
    }
}